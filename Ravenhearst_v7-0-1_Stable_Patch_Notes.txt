
Updated to Harmony2
Updated to Stable 19.0b180


Addition: Zombies now have random heights
Addition: Armor Bench Recipe
Addition: Darker Nights and Interiors Modlet (Already bracing for the response to this one)
Addition: Added Sinders Smoke Filled T5 Pois Indicating Radiation
Addition: Xyths Animal Pack with Crow, Spiders. May need some balancing.
Addition: Death Penalty is now more Harsh
Addition: Jerky, Salt and Trail Mix
Addition: Screws to Powered Recipes
Addition: Robears pallets
Addition: Admin XP and Skill Books
Addition: Set of Kukri
Addition: Baton
Addition: Archery, Blade, Blunt Class
Addition: Firearms Classes
Addition: Food Processor
Addition: Cling Wrap and Preservation Barrel
Addition: Journal with Pen and FULL Quest Line
Addition: Ink to Research recipes and loot
Addition: Poster and Journal items for opening quest
Addition: Seed Packs
Addition: Complete Journal Quest Line
Addition: Terrain and Road Buffs
Addition: Sharpened Stone
Addition: Dried tea Leaves
Addition: Composer and compost
Addition: Turds
Addition: Worms
Addition: Empty Gas Cans
Addition: new quests to opening quest
Addition: Sounds Folder in prep for custom sounds
Addition: Canned Water
Addition: Wave of Sinder redone rad Tier 5 pois with Smoke
Addition: Empty Gas Cans added in loot and craft
Addition: Wookies No Station Storage DLL and Module Checker
Addition: Fertilizer
Addition: Wookies Fix for Workstations. Fuel and Tools will now work but you cant place items in them
Addition: Additional Poster. More to come
Addition: Wookies Random Sound Dll. Now a sound cue will play randomly. Very much work in progress
Addition: Several POI fixes and sleeper placements by Sinder
Addition: New Meat Icons
Addition: Mumpfy New Zombie Textures (Biker, Joe, Janitor, Arlene, Darlene, Steve, Fat Mama)
Addition: Tormented Emu Texture DLL
Addition: New Personal Workbench Model
Addition: New Opening Book Model
Addition: Stone Well
Addition: Can Insects
Addition: Bug Kabob
Addition: Grey and Red Smoke Variants
Addition: Fixed Up Kuldin House


Balance: Table Saws in World Now Work
Balance: Replaced Stalactites with Salt Deposits
Balance: Fog should start soft in morning and get thicker by night
Balance: Scaled Spawns Back Slightly
Balance: Uncooked Insects now hit for 1 health
Balance: Removed Storage from Wood Helper
Balance: Added screws to Metal Helper Recipe
Balance: Replaced wood with Planks in Wood Helper Recipe
Balance: Garage Shelf Upgraded to 28 Slots for Storage
Balance: Increased repair times on all items
Balance: Reduced Amount of Zombies and Animals in CP for Performance
Balance: Changed Starter Crate
Balance: Rebalanced some books and updated descriptions
Balance: Reorganized and redid the Opening Quest.
Balance: Changed handles recipe
Balance: Compost and Fertilizer Added to Seed Recipe
Balance: Redid all station recipes so they are in line with reality and the journal
Balance: Increased XP Needed to level all action skills
Balance: Increased time on Pallet crafting
Balance: Infection persists through death
Balance: Death Penalty Increased. Now you lase 50 percent per death XP. Cap raised to 5 levels of xp loss so dont die!
Balance: Increased durability of all items
Balance: Removed T4 from damaging you to Radiation
Balance: Mixed up the RWG Mixer for vanilla and custom POI entries for more diversity in RWG worlds
Balance: Rad Suits Will No Longer Break
Balance: Bicycle can now be made in PWB
Balance: Updated New Default Settings to Play On
Balance: Infection will now persist through death
Balance: When you die you will now respawn with 60 percent food and water
Balance: Moved Chemistry Station to Physician 3
Balance: Moved various deco to decorating table
Balance: Moved some items out of PWB
Balance: wood frame recipes to 12 wood instead of planks
Balance: Lowered Physician Perk Benefits slightly
Balance: Lowered Lucky Looter Loot Times
Balance: Lowered Daring Adventurer Perks
Balance: Lowered Pain Tolerance Perk
Balance: Lowered amount needed to make forging plate
Balance: Rebar and Iron Frame Recipes
Balance: Increased Zombie Speeds
Balance: Thinned out Animals
Balance: Assigned Tiered Damage to Zombies
Balance: Heat Mapping on All Stations
Balance: Shortened Zombie Reach Slightly


Fix: Repair Kits are now properly named
Fix: AK Schematic Call removed in Action Skill
Fix: Insect Trap Cant Be Looted
Fix: Auger Harvesting
Fix: Wrench should now craft at proper quality
Fix: Loot Window Searching
Fix: Removed Empty Jar Mold
Fix: Rad Suit Resistance to radiation Increased
Fix: Fist Weapons Increasing Blunt Weapons Skill
Fix: Bone Fragments No Longer Scrap
Fix: Main Menu Updated
Fix: Tanning Rack and Survival Campfire added to PWB
Fix: Growth times on farming. It is now 120 a stage
Fix: Iron and Steel Arrowheads added to Working Stiffs
Fix: Grace Now Drops Proper Resources
Fix: Added Casket Helper
Fix: Yucca Smoothie now Cools You down
Fix: Icee Now Cools You Down
Fix: Storage Tags to Safes
Fix: Locked Research Desk
Fix: Many Localization and Icon Fixes
Fix: Wood Stove not showing survival campfire recipes
Fix: Open Quest Change
Fix: Composter now gives fertilizer
Fix: Gas Pumps will now downgrade when looted
Fix: Online requirement from Z2 removed
Fix: Gas pumps can now be upgraded with empty gas cans and looted over time
Fix: Lucky Looter times were incorrect
Fix: shotgun turret recipe asking for wrong parts
Fix: gun pieces in loot not being proper RH gun parts
Fix: Descriptions on Clothing and Armor to show Which Repair Kit is Needed
Fix: Leather Strips Scrapping
Fix: unlock info on salads
Fix: Stone Axes giving car parts


Removal: Cooler Recipe
Removal: Thumper :(  (Does not work on servers)
Removal: Iron Tools from PWB
Removal: Config File
Removal: Fishing Portion of Journal since its broken
Removal: Weather Modlets because they interfered with desired fog effect
Removal: Removed a few books
Removal: Seeds givebacks on Crops
Removal: Khaines Supply Crates
Removal: Food Spoilage
Removal: All newbie coats
Removal: Parkour
Removal: Steel Sledgehammer Parts from loot
Removal: Spoilage Text from Storages
Removal: Storage Pocket Mod Recipe Causing Nulls


