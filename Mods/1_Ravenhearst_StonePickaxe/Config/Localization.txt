﻿Key,Source,Context,English
meleeToolPickT0StonePickaxeRH,items,Tool,Stone Pickaxe,,,,,
meleeToolPickT0StonePickaxeRHDesc,items,Tool,,,"A primitive tool useful for mining stone. Not the best weapon.\nRepair with Small Stone.",,,,,
meleeToolRepairT0StoneAxe,items,Tool,,,Stone Axe,,Steinaxt,Hacha de piedra,Hache de pierre,Ascia di pietra,石斧,돌 도끼,Kamienna siekiera,Machado de Pedra,Каменный топор,Taş Balta,石斧,石斧,Hacha de piedra
meleeToolRepairT0StoneAxeRHDesc,items,Tool,,,"A primitive tool useful for cutting wood, repairing & upgrading walls, doors & windows. Not the best weapon.\nRepair with Small Stone.",,,,,

